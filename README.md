# psycho_chat_bot
## My first commerce project - freelance order completed in 2022. Reminds me of where I started.

The final project completed for a HSE student who underwent training on a Python course: a console survey bot for psychological testing. The topic was invented by the student herself, and the implementation was in accordance with the teacher's specifications.
___________________________________________________________________________________________

Итоговый проект, выполненный для студентки ВШЭ, которая прошла обучение на курсе по Python: консольный бот-опросник для психологического тестирования. Тематику придумал сам, реализация согласно ТЗ преподавателя.
